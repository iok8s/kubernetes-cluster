# Prometheus

Referencia: [https://gitlab.com/nanuchi/youtube-tutorial-series/-/blob/master/prometheus-exporter/install-prometheus-commands.md](https://gitlab.com/nanuchi/youtube-tutorial-series/-/blob/master/prometheus-exporter/install-prometheus-commands.md).

## Instalar Prometheus usando Helm

Se añade el repositorio

```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add stable https://charts.helm.sh/stable
helm repo update
```

Y se instala el helm-chart correspondiente:

```
helm install prometheus prometheus-community/kube-prometheus-stack -n develop
```

Este comando desplegará los siguientes pods:

```
 ~/gitlab/kubernetes > kubectl get pods | grep prometheus
alertmanager-prometheus-kube-prometheus-alertmanager-0   2/2     Running   0          2m35s
prometheus-grafana-bb6f77477-qhnzf                       2/2     Running   0          3m2s
prometheus-kube-prometheus-operator-68d59d8d59-28pkj     1/1     Running   0          3m2s
prometheus-kube-state-metrics-685b975bb7-8f924           1/1     Running   0          3m2s
prometheus-prometheus-kube-prometheus-prometheus-0       2/2     Running   1          2m32s
prometheus-prometheus-node-exporter-nswdn                1/1     Running   0          3m2s
```

Para acceder a la interfaz de Grafana se debe crear una regla de ingress sobre el puerto del nodo donde está desplegado. El ouerto se puede ver en los logs del pod de grafana, en este caso es el 3000:

```
 ~/gitlab/kubernetes > kubectl logs prometheus-grafana-bb6f77477-qhnzf -c grafana
...
t=2021-04-27T14:04:17+0000 lvl=info msg="HTTP Server Listen" logger=http.server address=[::]:3000 protocol=http subUrl= socket=
```

## Acceso a Grafana

Se expone la UI de Grafana creando un servicio sobre el pod de `prometheus-grafana`, que tiene una interfaz web en el puerto 3000:

```
 ~/gitlab/kubernetes > kubectl expose service prometheus-grafana --type=NodePort --target-port=3000 --name=prometheus-grafana-np
service/prometheus-grafana-np exposed
```

Para acceder se puede usar:

```
minikube service prometheus-grafana-np -n develop
```

El usuario por defecto es `admin` y la contraseña `prom-operator`.

![Grafana UI](images/grafana-ui.png)

## Acceso a la UI de Prometheus

Para acceder a la UI de Prometheus se puede proceder igual que con Grafana pero con el puerto 9090 y el servicio `prometheus-kube-prometheus-prometheus`:

```
 ~/gitlab/kubernetes > kubectl expose service prometheus-kube-prometheus-prometheus --type=NodePort --target-port=9090 --name=prometheus-server-np
service/prometheus-server-np exposed
```

Y exponer el servicio:

```
minikube service prometheus-server-np -n develop
```

![Prometheus UI](images/prometheus-ui.png)

## Configuración del datasource de Prometheus

<!-- Desde la interfaz de Grafana, se pulsa sobre Configuración->Data Sources->Add data source y se selecciona Prometheus. Solo se tiene que modificar la URL por `http://prometheus-server:80`. -->

Existen varios [dashboards públicos](https://grafana.com/grafana/dashboards), pero en este caso usaré [uno preparado para Kubernetes y Prometheus](https://grafana.com/grafana/dashboards/6417). Se pulsa sobre Create->Import y se introduce el ID de Grafana del dashboard. A continuación se selecciona el datasource que acabamos de configurar con Prometheus:

![Configuración del datasource](images/grafana-import.png)

Acto seguido deberían aparecer varias gráficas con estadísticas del clúster:

![Dashboard Grafana](images/grafana-dashboard.png)

## Añadir nuevos targets a Prometheus

Para que Promethus haga scrapping a un servicio dado necesita disponer de un servicemonitor (`kubectl get service monitor`) con `release: prometheus`. El servicemonitor se ha añadido a [collector.yml](collector.yml).

Para añadir un histograma en Grafana he seguido [esta](https://grafana.com/blog/2020/06/23/how-to-visualize-prometheus-histograms-in-grafana/) guía.

También se puede importar el [dashboard](dashboard.json) que he creado directamente.

## Añadir métricas de Kafka

Se modifica [kafka-persistent-single.yaml](kafka-persistent-single.yaml) incluyendo un `kafkaExporter` y tanto en `zookeeper` como en `kafka` una configuración `metricsConfig` de tipo `jmxPrometheusExporter`. A continuación se creará un pod `cluster-01-kafka-exporter-XXXXXXXXX-YYYYY` que expone en el puerto 9404 un endpoint `/metrics` que Prometheus puede consumir. Ahora bien, es necesario crear el servicio y el `servicemonitor`. Se definen en [kafka-exporter-service.yml](kafka-exporter-service.yml).

![Kafka dashboard](images/kafka-dashboard.png)