# Collector

Se crea el deployment y el service con

```
 ~/gitlab/kubernetes > kubectl apply -f collector.yml -n develop
deployment.apps/collector-deployment created
```

Se ha utilizado el puerto del nodo para poder probar a enviar peticiones. Minikube no tiene ingress habilitado por defecto. Para conocer la IP de Minikube, y por tanto el del nodo:

```
 ~/gitlab/kubernetes > minikube ip
192.168.49.2
```

El puerto del servicio en el nodo es el 30500. Para enviar dato se puede usar:

```
curl -k https://192.168.49.2:30500/collect -d '{"data": "hola", "id": "test"}'
```

Importante añadir `-k` para ignorar que el certificado es autofirmado. Se puede ver cómo van llegando los mensajes desde un consumidor:

```
kubectl -n develop run kafka-consumer -ti --image=quay.io/strimzi/kafka:0.22.1-kafka-2.7.0 --rm=true --restart=Never -- bin/kafka-console-consumer.sh --bootstrap-server cluster-01-kafka-bootstrap:9092 --topic topic_test --from-beginning
```