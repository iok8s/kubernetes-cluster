from kafka import KafkaConsumer  # Consumer de Kafka
import json                      # Para cargar el mensaje ya que está serializado en JSON

from matplotlib import pyplot as plt
import base64
import io
import matplotlib.image as mpimg

def base64_to_image(image):
    i = base64.b64decode(image.split(",")[1])
    i = io.BytesIO(i)
    i = mpimg.imread(i, format='JPG')
    return i

consumer = KafkaConsumer(
    'topic_test',
    bootstrap_servers=['192.168.49.2:30029'],
    auto_offset_reset='earliest',  # Al usar latest lee los mensajes que aún no se han consumido
    enable_auto_commit=True,       # para hacer commits periódicos de los offsets y no duplicar mensajes
    group_id='my-group-id',        # nombre del grupo de consumidores
    value_deserializer=lambda x: json.loads(x.decode('utf-8'))
)

for event in consumer:  # Para cada evento que llega al consumidor
    base46_img = event.value["security camera"]
    temperature = float(event.value["temperature"])
    humidity = int(event.value["humidity"])
    img = base64_to_image(base46_img)

    plt.imshow(img, interpolation='nearest')
    plt.title("{:.2f}℃, {}%".format(temperature, humidity))
    plt.xlabel("%s:%d:%d" % (event.topic, event.partition, event.offset))
    plt.xticks([])
    plt.yticks([])
    plt.pause(0.05)
plt.show()
