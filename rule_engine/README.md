# Redis 

Referencia: [marcel-dempers/docker-development-youtube-series](https://github.com/marcel-dempers/docker-development-youtube-series/tree/master/storage/redis/kubernetes).

## Configmap

La configuración de Redis se descarga desde la web oficial: [redis.conf](https://raw.githubusercontent.com/redis/redis/6.0/redis.conf) y se guarda en un ConfigMap para poder importarlo desde el deployment. Se escribe directamente en el fichero [redis-configmap.yaml](redis/redis-configmap.yaml) en el campo `data/redis.conf`.

### Autenticación

Se añade la contraseña `masterauth` (acceso desde API al máster) y `requirepass` (comunicación worker-máster) en las líneas 13 y 14 de [redis-configmap.yaml](redis/redis-configmap.yaml). Se utiliza la misma ya que si el pod máster se reinicia, un worker pasará a ser máster y habría que cambiar también la configuración. Utilizar la más simplifica la operación.

> Se aconseja modificar estas líneas o utilizar variables de entorno para una mayor seguridad. Se deben modificar también en [sentinel-statefulset.yaml](sentinel/sentinel-statefulset.yaml) y [engine.yaml](rest-api/engine.yaml)

### Almacenamiento

En la línea 373 se especifica el directorio donde se almacena la base de datos

```
dir "/data"
```

Este directorio será el que se utilice al montar el volumen persistente.

Como se recomienda en [http://redis.io/topics/persistence](http://redis.io/topics/persistence), se utiliza RDB + AOF. Se activa AOF en la línea 1102.


### Creación del configmap

Se usa el namespace `develop` para este ejemplo:

```
kubectl apply -n develop -f ./redis/redis-configmap.yaml
```

## Stateful set y Replicación

Es necesario configurar cada worker con la clave `slaveof` apuntando al máster de ese momento. Esto se debe hacer de forma dinámica ya que irá cambiando. Esto se hace en `initContainers` dentro de [redis-statefulset.yaml](redis/redis-statefulset.yaml).

Si no encuentra un nodo máster por defecto establece `redis-0`, pero si consigue comunicarse con Sentinel actualizará la configuración con el nombre de DNS del máster.

Es muy importante montar el volumen de `/etc/redis/`, donde se ha cargado el configmap, para que el contenedor pueda modificar el fichero de configuración con el nuevo valor de `slaveof`.

```
kubectl apply -n develop -f ./redis/redis-statefulset.yaml
```

## Sentinel

En [sentinel-statefulset.yaml](sentinel/sentinel-statefulset.yaml) se define un statefulset similar al anterior pero para desplegar el sentinel.

```
kubectl apply -n develop -f ./sentinel/sentinel-statefulset.yaml
```

Se configura para un clúster de tres nodos, correspondiente a las tres réplicas que se desplegaron con [redis-statefulset.yaml](redis/redis-statefulset.yaml).

```
nodes=redis-0.redis.redis.svc.cluster.local,redis-1.redis.redis.svc.cluster.local,redis-2.redis.redis.svc.cluster.local
``

## API REST

Por último se despliega la API rest:

```
kubectl apply -n develop -f rest-api/engine.yaml
```