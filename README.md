# Kubernetes

Se va a desplegar una [aplicación REST](https://gitlab.com/iok8s/collector) para recolección de mensajes de dispositivos, un clúster de Kafka para almacenarlos y Prometheus+Grafana para visualizar el estado de cada despliegue.

## Clúster de Kafka

Leer [desplegando un clúster de Kafka](strimzi.md) con el operador de Strimzi.

> **Opcional**: Para cambiar al namespace `develop` y evitar tener que añadir la flag `-n` en cada comando:
> ```
> kubectl config set-context --current --namespace=develop
> ```


## Collector

Leer [desplegando un pod con el collector](collector.md).

## Dashboard con métricas usando prometheus

Leer [desplegar y configurar Promehteus](prometheus.md).

## Rule Engine

Leer [desplegando un pod con el módulo de reglas](rule_engine/README.md).