# Operador de Strimzi

## Preparación del entorno

Descargo Minikube desde la web oficial e inicio un clúster local con 
```
 ~/gitlab/kubernetes > minikube start
😄  minikube v1.19.0 en Ubuntu 20.04
✨  Controlador docker seleccionado automáticamente
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
💾  Descargando Kubernetes v1.20.2 ...
    > preloaded-images-k8s-v10-v1...: 491.71 MiB / 491.71 MiB  100.00% 6.53 MiB
    > gcr.io/k8s-minikube/kicbase...: 357.67 MiB / 357.67 MiB  100.00% 2.25 MiB
🔥  Creando docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparando Kubernetes v1.20.2 en Docker 20.10.5...
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Complementos habilitados: storage-provisioner, default-storageclass
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

> Minikube usa por defecto 2GB. Si hiciera falta más añadir `--memory=4096`.

Se va a utilizar el operador [Strimzi](https://strimzi.io/). Se crea un namespace para ello con el operador de Strimzi:

```
 ~/gitlab/kubernetes > kubectl create namespace develop
namespace/develop created
 ~/gitlab/kubernetes > kubectl create -f 'https://strimzi.io/install/latest?namespace=develop' -n develop
Warning: apiextensions.k8s.io/v1beta1 CustomResourceDefinition is deprecated in v1.16+, unavailable in v1.22+; use apiextensions.k8s.io/v1 CustomResourceDefinition
customresourcedefinition.apiextensions.k8s.io/kafkas.kafka.strimzi.io created
rolebinding.rbac.authorization.k8s.io/strimzi-cluster-operator-entity-operator-delegation created
clusterrolebinding.rbac.authorization.k8s.io/strimzi-cluster-operator created
rolebinding.rbac.authorization.k8s.io/strimzi-cluster-operator-topic-operator-delegation created
customresourcedefinition.apiextensions.k8s.io/kafkausers.kafka.strimzi.io created
customresourcedefinition.apiextensions.k8s.io/kafkarebalances.kafka.strimzi.io created
deployment.apps/strimzi-cluster-operator created
customresourcedefinition.apiextensions.k8s.io/kafkamirrormaker2s.kafka.strimzi.io created
clusterrole.rbac.authorization.k8s.io/strimzi-entity-operator created
clusterrole.rbac.authorization.k8s.io/strimzi-cluster-operator-global created
clusterrolebinding.rbac.authorization.k8s.io/strimzi-cluster-operator-kafka-broker-delegation created
rolebinding.rbac.authorization.k8s.io/strimzi-cluster-operator created
clusterrole.rbac.authorization.k8s.io/strimzi-cluster-operator-namespaced created
clusterrole.rbac.authorization.k8s.io/strimzi-topic-operator created
clusterrolebinding.rbac.authorization.k8s.io/strimzi-cluster-operator-kafka-client-delegation created
clusterrole.rbac.authorization.k8s.io/strimzi-kafka-client created
serviceaccount/strimzi-cluster-operator created
clusterrole.rbac.authorization.k8s.io/strimzi-kafka-broker created
customresourcedefinition.apiextensions.k8s.io/kafkatopics.kafka.strimzi.io created
customresourcedefinition.apiextensions.k8s.io/kafkabridges.kafka.strimzi.io created
customresourcedefinition.apiextensions.k8s.io/kafkaconnectors.kafka.strimzi.io created
customresourcedefinition.apiextensions.k8s.io/kafkaconnects2is.kafka.strimzi.io created
customresourcedefinition.apiextensions.k8s.io/kafkaconnects.kafka.strimzi.io created
customresourcedefinition.apiextensions.k8s.io/kafkamirrormakers.kafka.strimzi.io created
configmap/strimzi-cluster-operator created
```

Es muy importante incluir en la url el parámetro `namespace=develop` con el nombre del namespace con el que se trabaje, ya que generará el archivo de configuración en base a ello. Se trabaja siempre sobre `develop` salvo si hay que probar algo concreto, pero no es recomendable usar varios namespaces para un mismo proyecto porque muchos pods implican mucha memoria.

## Despliegue de un clúster de Kafka de un solo nodo

Strimzi tiene varios ejemplos para desplegar con un solo comando. Para disponer de un clúster de Kafka con un solo broker, basta con ejecutar:

```
kubectl apply -f https://strimzi.io/examples/latest/kafka/kafka-persistent-single.yaml -n develop 
```

Este ejemplo utiliza un volumen persistente de 100GiB. Realmente, para este ejemplo con 10GiB debería sobrar (5 para Kafka y otros 5 para Zookeeper). Así que he descargado el [fichero](kafka-persistent-single.yaml) y modificado esa parte, además de cambiar el nombre del clúster por `cluster-01`. En lugar de utilizar el enlace de Strimzi se utilizaría entonces el local:

```
 ~/gitlab/kubernetes > kubectl apply -f kafka-persistent-single.yaml -n develop 
kafka.kafka.strimzi.io/cluster-01 created
```

Y esperar a que todos los recursos estén disponibles:

```
 ~/gitlab/kubernetes > kubectl wait kafka/cluster-01 --for=condition=Ready --timeout=300s -n develop
kafka.kafka.strimzi.io/cluster-01 condition met
```

> Si la conexión es muy mala puede que tarde más de 300s en descargar las imágenes de los contenedores y haya que volver a ejecutar este comando, o aumentar el timeout.

Una vez finalizado deberían estar disponibles 4 pods:

```
 ~/gitlab/kubernetes > kubectl get pods -n develop
NAME                                         READY   STATUS    RESTARTS   AGE
cluster-01-entity-operator-9cd7567d9-gtslr   3/3     Running   2          4m42s
cluster-01-kafka-0                           1/1     Running   0          5m9s
cluster-01-kafka-exporter-7dd6fc8f46-g7ggb   1/1     Running   1          5m43s
cluster-01-zookeeper-0                       1/1     Running   0          6m32s
strimzi-cluster-operator-957688b5c-45w98     1/1     Running   0          16m
```

A parte del broker de Kafka, el servidor de Zookeeper y el operador de Strimzi, se crean 3 pods de tipo `entity-operator`. Deberían ser el User Operator, el Topic Operator y el Cluster Operator.

Para producir y consumir mensajes se podrían usar estos comandos respectivamente:

```
kubectl -n develop run kafka-producer -ti --image=quay.io/strimzi/kafka:0.22.1-kafka-2.7.0 --rm=true --restart=Never -- bin/kafka-console-producer.sh --broker-list cluster-01-kafka-bootstrap:9092 --topic my-topic


kubectl -n develop run kafka-consumer -ti --image=quay.io/strimzi/kafka:0.22.1-kafka-2.7.0 --rm=true --restart=Never -- bin/kafka-console-consumer.sh --bootstrap-server cluster-01-kafka-bootstrap:9092 --topic my-topic --from-beginning
``` 

Ejecutar estos comandos crea nuevos pods de tipo `kafka-producer` y `kafka-consumer` respectivamente.